module.exports = function(app) {
    var mysql = require('mysql');//pour reqûetes

    var pool = mysql.createPool({
        connectionLimit: 100,
        multipleStatements: true,
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'fcommonbdd' //nom de la bdd
    });

    app.get('/api/routeSomething/', getSomething); //route Get qui lance getSomthing

   
    function getSomething(req, res, next) {
        pool.getConnection(function (err, connection) {    
            connection.query('select * from sometable', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }


};