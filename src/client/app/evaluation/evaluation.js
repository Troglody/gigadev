(function () {
    'use strict';

    angular
        .module('app.evaluation')
        .controller('Evaluation', Evaluation);

    /* @ngInject */
    function Evaluation(dataservice, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'À vous de jouer !';

        vm.quizzData = [];
        vm.answers = [];
        vm.questions = [];
        vm.questionNbr = 0;
        vm.bonneRep = [];
        vm.haderror = false;
        vm.errorCpt = 0;
        vm.onceScore = 0;


        vm.LoadBtn = LoadBtn;
        vm.LoadQuizz = LoadQuizz;
        vm.StartQuizz = StartQuizz;
        // vm.RetryQuizz = RetryQuizz;

        vm.TerminerRefresh = TerminerRefresh;

        //  $auth.logout();
        activate();

        function activate() {
            var promises = [];
            return dataservice.ready(promises).then(function () {
                // logger.info('Evaluation OK');

            })
        }


        function LoadBtn(event) {

            var element = event.currentTarget;
            var body = $('#BtnModalBody');
            var elID = element.id;


            switch (elID) {
                case "html":
                    body.html('<button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'HTMLQ1\')">HTML | Quizz 1 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'HTMLQ2\')">HTML | Quizz 2 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'HTMLQ3\')">HTML | Quizz 3 </button>').fadeIn("slow");
                    break;

                case "css":
                    body.html('<button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CSSQ1\')">CSS | Quizz 1 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CSSQ2\')">CSS | Quizz 2 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CSSQ3\')">CSS | Quizz 3 </button>');
                    break;

                case "js":
                    body.html('<button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'JSQ1\')">JS | Quizz 1 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'JSQ2\')">JS | Quizz 2 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'JSQ3\')">JS | Quizz 3 </button>');

                    break;

                case "c":
                    body.html('<button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CppQ1\')">C++ | Quizz 1 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CppQ2\')">C++ | Quizz 2 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CppQ3\')">C++ | Quizz 3 </button>');

                    break;

                case "vbnet":
                    body.html('<button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'VBnetQ1\')">VB | Quizz 1 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'VBnetQ2\')">VB | Quizz 2 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'VBnetQ3\')">VB | Quizz 3 </button>');
                    break;

                case "c-sharp":
                    body.html('<button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CsharpQ1\')">C# | Quizz 1 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CsharpQ2\')">C# | Quizz 2 </button><button data-toggle="modal" data-target="#QuizzModal" type="button" class="btn btn-primary btn-custom" ng-click="vm.LoadQuizz(\'CsharpQ3\')">C# | Quizz 3 </button>');
                    break;

                default:
                    break;

            }
            Compile(body);
        }


        function LoadQuizz(whichQuizz) {
            $('.winscreen').hide();

            return dataservice.getQuizz(whichQuizz).then(function (data) {
                vm.quizzData = data;
                vm.questions = vm.quizzData.questions;
                vm.answers = vm.quizzData.answers;
                vm.bonneRep = vm.quizzData.bonneRep;
            });
        }


        function StartQuizz(labelindex) {

            var loading = $('#loadbar').hide();
            $(document)
                .ajaxStart(function () {
                    loading.show();
                }).ajaxStop(function () {
                    loading.hide();
                });

            var ans = vm.bonneRep[vm.questionNbr];


            function checking(val) {
                if (parseInt(val) != parseInt(ans)) {
                    document.getElementById('answer').innerHTML = '<div id="WinAlert" class="alert alert-danger" role="alert"> Essaye encore </div>';
                    vm.haderror = true;
                } else {
                    document.getElementById('answer').innerHTML = '<div id="WinAlert" class="alert alert-success" role="alert"> Bravo! </div>';
                    if (vm.questionNbr + 1 != vm.questions.length) { //si le numéro de la question est < au total des questions ( s'il en reste )
                        NextQuestion();
                        if (vm.haderror) { //si il y a eu des erreurs sur LA question
                            vm.errorCpt += 1; //l'ajouter au compteur
                            vm.haderror = false; // reset le booléen
                        }
                    } else {
                        FinishQuizz();
                    }
                }
                $('#answer').hide();
                $('#answer').show(100);
            };

            checking(labelindex); //check bonnereponse

        };


        function NextQuestion() {
            $('#loadbar').show();
            $('#quiz').fadeOut();
            vm.questionNbr += 1;
            setTimeout(function () {
                $('#quiz').show();
                $('#loadbar').fadeOut();
            }, 500);
        }

        // function RetryQuizz() {
        //     TerminerRefresh();
           
        // }


        function FinishQuizz() {

            var win = document.getElementsByClassName('winscreen');
            $(win).fadeIn(600);
            vm.onceScore = vm.questions.length - vm.errorCpt; //score total = total des question - questions avec erreurs
            $('#quiz').fadeOut(300);
            document.getElementById('answer').innerHTML = '<br><div id="WinAlert" class="winscreen alert alert-warning " role="alert" style="margin-right: 300px;"> Félicitations!</div>' + '<button id="TerminerBtn" type="button" ng-click="vm.TerminerRefresh()" class="btn btn-primary " data-dismiss="modal">Terminer</button>'; // + '<button type="button" id="retryBtn" class="btn btn-secondary" ng-click="vm.RetryQuizz()">Réessayer</button>';
            Compile($('#TerminerBtn'));
            Compile($('#retryBtn')); // Recompile après l'ajout de ng-click dans le retryBtn

            document.getElementById('qidh3').innerHTML = "<span class='label label-warning' id='qid'> Quizz terminé! </span>";
        }

        function TerminerRefresh() {
            vm.quizzData = [];
            vm.answers = [];
            vm.questions = [];
            vm.questionNbr = 0;
            vm.bonneRep = [];
            vm.haderror = false;
            vm.errorCpt = 0;
            vm.onceScore = 0;
            $('.modal').hide();
            $('.modal-backdrop').hide();
            var element = $('#FinishRefresh');
            console.log(element);
            Compile(element);

        }


        function Compile(element) {

            console.log("compiled");
            var el = angular.element(element);
            Evaluation = el.scope();
            var injector = el.injector();
            injector.invoke(function ($compile) {
                $compile(el)(Evaluation)
            })
        }

    }


})();

// function checking(choice) {
//     if (choice.trim() != ans.trim()) {
//         return '<div class="alert alert-danger" role="alert"> Essaye encore </div>';
//     } else {
//         return '<div class="alert alert-success" role="alert"> Bravo! </div>';
//     }
// };



/*var variable;
var variable = 2;
var variable2 = "chaine de caractère";
var variable3 = parseInt(prompt());
var variable4 = true;

for (i=0; i<14; i++) {
    variable++
}
while (varibale = 2){
    variable4 = false;
}*/