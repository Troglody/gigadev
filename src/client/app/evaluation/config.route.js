(function() {
    'use strict';

    angular
        .module('app.evaluation')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/evaluation',
                config: {
                    templateUrl: 'app/evaluation/evaluation.html',
                    controller: 'Evaluation',
                    controllerAs: 'vm',
                    title: 'Evaluation',
                    settings: {
                        nav: 2,
                        content:'<i class="fas fa-pencil-alt"></i> Evaluation'
                    }
                }
            }
        ];
    }
})();
