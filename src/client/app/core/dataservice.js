(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    /* @ngInject */
    function dataservice($http, $location, $q, exception, logger) {
        var isPrimed = false;
        var primePromise;

        var service = {
            ready: ready,
            getQuizz: getQuizz,
            getCours: getCours
        };

        return service;

        
      

        function getQuizz(whichQuizz) {
            
            return $http.get('http://localhost:7203/src/client/content/JSONQuizz/'+whichQuizz+'.json')
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }


        function getCours(whichCours) {
            
            return $http.get('http://localhost:7203/src/client/content/JSONCours/'+whichCours+'.json')
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }





 

        function prime() {
            // This function can only be called once.
            if (primePromise) {
                return primePromise;
            }

            primePromise = $q.when(true).then(success);
            return primePromise;

            function success() {
                isPrimed = true;
                // logger.info('Dataservice Primed data');
            }
        }

        function ready(nextPromises) {
            var readyPromise = primePromise || prime();

            return readyPromise
                .then(function() { return $q.all(nextPromises); })
                .catch(exception.catcher('"ready" function failed'));
        }

    }
})();
