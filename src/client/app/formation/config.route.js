(function() {
    'use strict';

    angular
        .module('app.formation')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/formation',
                config: {
                    templateUrl: 'app/formation/formation.html',
                    controller: 'Formation',
                    controllerAs: 'vm',
                    title: 'Formation',
                    settings: {
                        nav: 1,
                        content: '<i class="fas fa-graduation-cap"></i> Formation'
                    }
                }
            }
        ];
    }
})();
