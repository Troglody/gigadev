(function () {
    'use strict';

    angular
        .module('app.formation')
        .controller('Formation', Formation);

    /* @ngInject */
    function Formation(dataservice, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'Que voulez vous apprendre ?';
        vm.something = [];

        vm.cours = [];
        vm.LoadCours = LoadCours;

        //  $auth.logout();

        activate();

        function activate() {
            var promises = [];
            return dataservice.ready(promises).then(function () {
                // logger.info('Formation OK');
            })
        }


        function LoadCours(whichCours) {

            return dataservice.getCours(whichCours).then(function (data) {
                vm.cours = data;

         
                $('#Fond').html('<div id="insertCours"></div>');
                $('#insertCours').html('<div ng-include="\'src/client/app/CoursBase.html\'"></div>');
                Compile($('#Fond'));
            });
        }


        function Compile(element) {
            var el = angular.element(element);
            Formation = el.scope();
            var injector = el.injector();
            injector.invoke(function ($compile) {
                $compile(el)(Formation)
            })
        }

    }
})();